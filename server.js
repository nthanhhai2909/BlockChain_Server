var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var mongoose = require('mongoose');
var cors = require('cors');
var path = require('path');
var bodyParser = require('body-parser');
var WebSocket = require('./api/utils/WebSocket');
var transactionController = require('./api/controllers/TransactionController')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var User = require('./api/models/UserModel'); // create model User loading here
var TokenVerified = require('./api/models/TokenVerifiedModel'); // create model Token loading here
var KPA = require('./api/models/KeyPairAddressModel'); // create model KPA loading here
var block = require('./api/models/BlockModel'); // create model KPA loading here
var transaction =  require('./api/models/TransactionModel');
var contract = require('./api/models/ContractModel');
var transactionUnconfirm  = require('./api/models/TransactionUnconfirmModel');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cors({ credentials: true, origin: 'http://localhost:3001' }));
app.get('/', (req, res) => {
    res.render("index");
})

//-- begin - auth--
var userController = require('./api/controllers/UserController');


var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var cookieParser = require('cookie-parser');
var session = require('express-session');

app.use(session({secret: 'mysecrect',
cookie:{
    maxAge: 1000*60*30
}}));

passport.use(new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
},
    (username, password, done) => {
        userController.loginUser(username, password, done);
    }
))


passport.serializeUser((data, done) => {
    
    done(null, data);

})

passport.deserializeUser((dataInput, done) => {
    // console.log(dataInput);
    // user.getUserByEmail(dataInput, done);
    done(null, dataInput);
})
// Passport init
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());


app.get('/loginOK', (req, res) => res.json({status:'true'}));
app.get('/loginFail', (req, res) => res.json({status: 'false'}));
app.get('/logout', (req, res) => {
      req.logout();
      res.json({status: 'true'});
  });

  app.get('/authentication', (req, res) => {
      if(req.isAuthenticated()){
        if(req.user.email === "pnhai.vn@gmail.com"){
            res.send({status:'true', user: req.user, type:1})
          }
          else{
            res.send({status:'true', user: req.user, type:0})
          }

      }
      else{
          res.send({status:'false'})
      }
  });
//-- end - auth--
app.post('/admin/*', ensureAuthenticated, (req, res, next) => {
    return next();
});

function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated() && (req.user.email === "pnhai.vn@gmail.com")){
        return next();
    } else {
        res.send({status:'false'})
    }
}




mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/bitcoindb');

var userRouter = require('./api/routers/UserRouter');
var keyPairAddress = require('./api/routers/KeyPairAddressRouter');
var adminRouter = require('./api/routers/AdminRouter');
adminRouter(app)
keyPairAddress(app);
userRouter(app, passport);
WebSocket.syncKcoinBlockChain();
WebSocket.serverListen();
const init = require('./api/utils/init');
init.dataSynchronization();
app.listen(port, () => console.log("Server running on port " + port));