'use strict'

module.exports = (app, passport) => {
    var userController = require('../controllers/UserController');
    var transactionController = require('../controllers/TransactionController');
    app.route('/register')
    .post(userController.register);
    
    app.route('/user/confirmation/:token')
    .get(userController.confirmation);

    app.route('/login')
    .post(passport.authenticate('local', {successRedirect:'/loginOK', failureRedirect:'/loginFail'})); 

    app.route('/user/forgotpassword')
    .post(userController.fotgotPassword)

    app.route('/user/resetpassword')
    .post(userController.resetpassword)

    app.route('/user/:email')
    .get(userController.getAddress)

    app.route('/user/confirmtransaction/:token')
    .get(userController.cofirmSentKcoin)
    
    app.route('/user/balance/:email')
    .get(userController.getBalance)

    app.route('/user/sentKCoin')
    .post(userController.sentKcoin)

    app.route('/user/transactionunconfirm/')
    .post(transactionController.getTransactionUnconfirm)

    app.route('/user/gettransactionreivece/')
    .post(transactionController.getTransactionReivece);

    app.route('/user/gettransactiontranfer/')
    .post(transactionController.getTransactionTranfer);

    app.route('/user/delete/transactionunconfirm/:token')
    .get(transactionController.deleteTransactionUnconfirm)

}