'use strict'

module.exports = app => {
    var adminController = require('../controllers/AdminController');

    app.route('/admin/alluser')
    .get(adminController.getAmountAllUser);

    app.route('/admin/getactualbanlancesystem')
    .get(adminController.getActualBalanceSystem)

    app.route('/admin/getavailablebanlancesystem')
    .get(adminController.getAvailableBalanceSystem)

    app.route('/admin/getinforalluser')
    .post(adminController.getInforAllUser)

    app.route('/admin/getTransactionUnconfirm')
    .post(adminController.getTranSactionUnconfirm)

    app.route('/admin/getinforalladdressinternal')
    .post(adminController.getInforAllAddressInternal)

    app.route('/admin/getinforaddress/:address')
    .get(adminController.getInforAddress)
}