'use strict'

module.exports = app => {
    var keyPairAddressController = require('../controllers/KeyPairAddressController');
    
    app.route('/kpa/add')
    .post(keyPairAddressController.addKeyPairforUser);
    
}