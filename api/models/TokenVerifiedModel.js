'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var tokenVerified = new Schema({
    _userID: { type: mongoose.Schema.Types.ObjectId,required: true, ref: 'user'},
    token: { type: String, required: true },
});
module.exports = mongoose.model('token', tokenVerified);