'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var block = new Schema({
    hash: { type: String},
    nonce: { type: Number},
    version: { type: Number},
    timestamp: { type: Number},
    difficulty: { type: Number},
});
module.exports = mongoose.model('block', block);
