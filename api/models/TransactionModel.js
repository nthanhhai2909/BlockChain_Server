'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var transaction = new Schema({
    blockHash: { type: String},
    hash: { type: String},
    inputs: {type: Array},
    outputs: {type: Array}
});

module.exports = mongoose.model('transaction', transaction);