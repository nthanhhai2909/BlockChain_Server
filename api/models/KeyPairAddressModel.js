'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var kPA = new Schema({
    _userID: { type:String},  
    publicKey: { type:String},   
    privateKey: {type:String},
    address: {type:String},
});

module.exports = mongoose.model('keypairaddress', kPA);