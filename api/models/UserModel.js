'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var User = new Schema({
    email: { type:String,required: true, unique: true},   
    password: {type:String},
    isVerified: { type: Boolean, default: false },
    actualBalance: {type: String, default: '0'},
    availableBalance: {type: String, default: '0'}
    
});
User.plugin(mongoosePaginate);


module.exports = mongoose.model('user', User);