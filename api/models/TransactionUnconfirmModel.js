
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var transactionUnconfirm = new Schema({
    token: { type: String},
    isVerified: { type: Boolean, default: false },
    depositInformations: {type: Array}, // address - c
    _userID: {type: String},
    status: {type: Number},
    outputIndex: {type: Number, default: -1},
    hashTransactionID: {type: String, default: 'undefined'},
    date: {type: Date, default: new Date() }
    //status: 1 - khoi tao 2 - dang xu ly 3 - hoan thanh
});

module.exports = mongoose.model('transactionUnconfirm', transactionUnconfirm);