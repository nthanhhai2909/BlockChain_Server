'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var contract = new Schema({
    address: { type: String},
    hashTransactionID: { type: String},
    outputIndex: { type: Number},
    amount: { type: String},
    isUse: {type: Boolean, default: false},
    date: {type: Date, default: new Date() },
    awaitConfirm : {type: Boolean, default: false}
});

module.exports = mongoose.model('contract', contract);