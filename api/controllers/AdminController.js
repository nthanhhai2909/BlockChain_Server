'use strict'
var mongoose = require('mongoose');
var User = require('../models/UserModel');
var transactionUtils = require('../utils/transaction');
var contract = require('../models/ContractModel');
var transactionUnconfirm = require('../models/TransactionUnconfirmModel');
var kPA = require('../models/KeyPairAddressModel');
var userController = require('../controllers/UserController');
exports.getAmoutUser = () => {
    return new Promise(resolve => {
        resolve(User.count({}))
    })

}
exports.getAmountAllUser = async (req, res) => {
    let amount = await this.getAmoutUser();
    res.json({status: 'true', data: amount})
}


exports.getActualBalanceSystem = async (req, res) => {
    let contracts = await transactionUtils.getALlContractUse();
    let total = 0;
    for(let i = 0; i<  contracts.length; i++){
        for(let j = 0; j < contracts[i].length; j++ ){
            total += parseFloat(contracts[i][j].amount);
        }
       
    }
    res.json({status: 'true',data: total})    
}

exports.getAvailableBalanceSystem = async (req, res) => {
    let contracts = await transactionUtils.getALlContractUse();
    let total = 0;
    for(let i = 0; i<  contracts.length; i++){
        for(let j = 0; j < contracts[i].length; j++ ){
            if(contracts[i][j].awaitConfirm === false){
                total += parseFloat(contracts[i][j].amount);
            }
            
        }
    }
    res.json({status: 'true',data: total})    
}

exports.getInforAllUser = async (req, res) => {
    if(typeof req.body.amount === 'undefined'
        || typeof req.body.page === 'undefined'){
            res.json({status: 'false'})
            return;
    }
    let mypage = parseInt(req.body.page);
    if(mypage < 1){
        res.json({status: 'false', mess: 'page < 0'});
        return;
    }

    let myamount = parseInt(req.body.amount);
    let skip = (mypage-1) * myamount; 
    let allRecord = await this.getAmoutUser(); 
    let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
    if(mypage > mtotalPage){
        res.json({status: 'false', message: 'page > toal'});
        return;
    }
    
    let myData = [];
    kPA
    .find({})
        .limit(myamount)
        .skip(skip)
        .exec(async function(err, docs){
            if(err){
                res.json({status:'false'});
            }
            for(let i = 0; i < docs.length; i++){
                let userRes = await userController.findUserByID(docs[i]._userID);
                let obj = {
                    email: userRes.email,                    
                    address: docs[i].address,
                    userID: docs[i]._userID,
                    actualBalance: userRes.actualBalance,
                    availableBalance: userRes.availableBalance,
                }
                myData.push(obj);
            }

            res.json({status: 'true', data: myData, totalPage: mtotalPage});
        })

}

exports.findKPAByAddress = (maddress) => {
    return new Promise(resolve => {
        kPA.findOne({address: maddress}, {}, (err, docs) => {
            if(err){
                resolve(false);
                return;
            }
            if(docs === null){
                resolve(false);
                return;
            }
            resolve(docs)
        })
    })
}

exports.getInforAddress =async (req, res) => {
    if(typeof req.params.address === 'undefined'){
        res.json({status: 'false', mess: 'input invalid'});
        return;
    }

    let kpaRes = await this.findKPAByAddress( req.params.address);
    if(kpaRes !== false){
        var userRes = await userController.findUserByID(kpaRes._userID);
        console.log('user', userRes)
        res.json({status: 'true', balance: userRes.actualBalance });
        return;
    }
    contract.find({address: req.params.address, isUse: false, awaitConfirm : false}, {}, (err, docs) =>{
        if(err){
            res.json({status: 'false', mess: 'input invalid'});
            return; 
        }
        if(docs.length === 0){
            res.json({status: 'true', balance:0 });
            return; 
        }
        let mbalance = 0;
        for(let i = 0; i < docs.length; i++){
            console.log('data', mbalance)
            mbalance += parseFloat(docs[i].amount);
            console.log('data', mbalance)
        }
        res.json({status: 'true', balance:mbalance });
            return; 

    });
}

exports.getTotalTransactionUnconfirm = () => {
    return new Promise (resolve => {
        resolve(transactionUnconfirm.count({}));
    })
}

exports.getTotalAddressSystem = () => {
    return new Promise(resolve => {
        resolve(kPA.count({}));
    })
}
// exports.getTotalAddress 
exports.getInforAllAddressInternal = async (req, res) => {
    if(typeof req.body.amount === 'undefined'
        || typeof req.body.page === 'undefined'){
            res.json({status: 'false'})
            return;
    }
    let mypage = parseInt(req.body.page);
    if(mypage < 1){
        res.json({status: 'false', mess: 'page < 0'});
        return;
    }

    let myamount = parseInt(req.body.amount);
    let skip = (mypage-1) * myamount; 
    let allRecord = await this.getTotalAddressSystem(); 
    let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
    if(mypage > mtotalPage){
        res.json({status: 'false', message: 'page > toal'});
        return;
    }

    
    let myData = [];
    kPA
    .find({})
        .limit(myamount)
        .skip(skip)
        .exec(async function(err, docs){
            if(err){
                res.json({status:'false'});
            }
            for(let i = 0; i < docs.length; i++){
                let userRes = await userController.findUserByID(docs[i]._userID);
                let obj = {
                    address: docs[i].address,
                    userID: docs[i]._userID,
                    actualBalance: userRes.actualBalance,
                    availableBalance: userRes.availableBalance,
                }
                myData.push(obj);
            }

            res.json({status: 'true', data: myData, totalPage: mtotalPage});
        })

}

exports.getTranSactionUnconfirm = async (req, res) => {
    if(typeof req.body.amount === 'undefined'
        || typeof req.body.page === 'undefined'){
            res.json({status: 'false'})
            return;
    }

    let mypage = parseInt(req.body.page);
    if(mypage < 1){
        res.json({status: 'false', mess: 'page < 0'});
        return;
    }

    let myamount = parseInt(req.body.amount);
    let skip = (mypage-1) * myamount; 
    let allRecord = await this.getTotalTransactionUnconfirm(); 
    let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
    if(mypage > mtotalPage){
        res.json({status: 'false', message: 'page > toal'});
        return;
    }
    transactionUnconfirm
    .find({})
        .limit(myamount)
        .skip(skip)
        .exec(async function(err, docs){
            if(err){
                res.json({status: 'false'});
            }
            let mydata = [];
            for(let i = 0; i < docs.length; i++){
                let userRes = await userController.findUserByID(docs[i]._userID);
                let obj = {
                    email: userRes.email,
                    depositInformations: docs[i].depositInformations,
                    status: docs[i].status,
                    outputIndex: docs[i].outputIndex,
                    hashTransactionID: docs[i].hashTransactionID,
                }
                mydata.push(obj);
            }
            res.json({status: 'true', data: mydata, totalPage: mtotalPage});
        })
}
