﻿﻿'use strict'
var mongoose = require('mongoose');
var User = require('../models/UserModel');
var tokenVerified = require('../models/TokenVerifiedModel');
var kPA = require('../models/KeyPairAddressModel');
var randomstring = require("randomstring");
var passwordHash = require('password-hash');
const nodemailer = require('nodemailer');
var axios = require('axios')
var transporter = nodemailer.createTransport('smtps://nthanhhai2909%40gmail.com:missing456@smtp.gmail.com');
var transactionUtils = require('../utils/transaction');
var contract = mongoose.model('contract');
var transactionUnconfirm = mongoose.model('transactionUnconfirm');
var transactionController = require('./TransactionController')
exports.register = async (req, res) => {
    
    var kpaRes = await axios.get('https://api.kcoin.club/generate-address')

    if((typeof req.body.email == 'undefined')
         || (typeof req.body.password == 'undefined')
            || typeof req.body.confirm == 'undefined'){
        res.json({status: 'false', message: 'INPUT INVALID'});
        return;
    }

    if(req.body.email.length < 6 || req.body.password < 6){
        res.json({status: 'false', message: 'INPUT INVALID'});
        return;
    }

    let hashedPassword = passwordHash.generate(req.body.password);
    User.findOne({'email': req.body.email}, 'email', (err, user) => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
    
        if(user !== null){
            res.send({status: 'false', message: "User Already exsit"});
            return;
        }
    
        if(req.body.password != req.body.confirm){
            res.json({status: 'false', message:"INPUT INVALID"});
            return;
        }
        var new_user = new User({
            email: req.body.email,
            password: hashedPassword,
        });

        new_user.save((err, data) =>{
            if(err){
                res.json({status: 'false', message:err});
                return;
            }
            var newKPA = new kPA({
                _userID: data._id,  
                publicKey: kpaRes.data.publicKey,   
                privateKey: kpaRes.data.privateKey,
                address: kpaRes.data.address,
            });
            newKPA.save((err, newkPA) => {
                if(err){
                    res.json({status: 'false', message: err});
                    return;
                }
            })

            var newToken = new tokenVerified({
                _userID: data._id,
                token: randomstring.generate(),
            })
            // create reusable transporter object using the default SMTP transport
            

            newToken.save((err, token) => {
                if(err){
                    res.json({status: 'false', message:err});
                    return;
                }
                // setup email data with unicode symbols
                let mailOptions = {
                    from: '"NTHANHHAI 👻" <nthanhhai2909@gmail.com>', // sender address
                    to: req.body.email, // list of receivers
                    subject: 'Account Verification Token', // Subject line
                    text: 'Hello,\n\n' + 'Please verify your account by clicking the link: \n\nhttp:\/\/' + 'localhost:3000'
                            + '\/confirmation\/' + token.token + '.\n',
                    html: '<b>verify your account</b>'
                        + ' <br/>'
                        + '<span>Please verify your account by clicking the link</span>'
                        + '<br/>'
                        + '<span>http://localhost:3001/confirm/' + token.token +  '</span>'
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        res.json({status: 'false', message:error});      
                        return;
                    }
                    res.json({status: 'true', message:'', _userID: token._userID});
                });
            })

            
        });   

    });
}

exports.confirmation = (req, res) => {
    req.params.token = req.params.token.trim();
    if(typeof req.params.token === 'undefined'){
        res.json({status: 'false', message: 'INPUT INVALID'});
        return;
    }
    tokenVerified.findOne({'token': req.params.token}, {}, (err, data) => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
        
        if(data === null){
            res.json({status: 'false', message: 'INPUT INVALID'});
            return;
        }

        User.findById(String(data._userID), (err, user) => {
            if(err){
                res.json({status: 'false', message: err});
                return;
            }
            if(user === null){
                res.json({status: 'false', message: 'INPUT INVALID'});
                return;
            }

            user.isVerified = true;

            user.save((err, data) => {
                if(err){
                    res.json({status: 'false', message: err});
                    return;
                }
                res.json({status: 'true', message: ''});

            })
        });
    })

}

exports.loginUser = function (username, password, done) {
	User.findOne({'email': username},  (err, data)=>{
		
		if(err){
			res.send(err);
		}
		else{
			if(data === null){
				return done(null, false);
			}
			else{
                if(data.isVerified === false){
                    return done(null, false);
                }
				let hashedPassword = data.password;
				let res = Object.assign({}, {username: username, password: password})
				if(passwordHash.verify(password, hashedPassword))
				{
					return done(null, data);
				}
				else{
					return done(null, false);
				}     
					
			}
		}
	});
};
exports.login = (req, res) => {
    if((typeof req.body.email == 'undefined')
         || (typeof req.body.password == 'undefined')){
        res.json({status: 'false', message: 'INPUT INVALID'});
        return;
    }

    if(req.body.email.length < 6 || req.body.password < 6){
        res.json({status: 'false', message: 'INPUT INVALID'});
        return;
    }

    User.findOne({'email': req.body.email}, (err, user) => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
        if(user != null)
        {

            console.log(req.body.password);
            console.log(user.password);
            console.log(user);
            if(passwordHash.verify(req.body.password, user.password))
            {
                if(user.isVerified === false){
                    res.json({status: 'false', message:"ACCOUNT HAS NOT CONFIRMED EMAIL"});
                }
                else{
                    res.json({status: 'true', message:"LOGIN SUCCESS", data: user});
                }
                
            }
            else{
                res.json({status: 'false', message:"ERROR PASSWORD"});
            }
        }else{
            res.json({status: 'false', message:"NOT EXIST USER"});
        }
    });
}

exports.fotgotPassword = (req, res) => {
    if(typeof req.body.email ===  'undefined'){
        res.json({status: 'false'})
    }
    User.findOne({'email': req.body.email}, 'email', (err, user) => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
        if(user === null){
            res.json({status: 'false'});
            return;
        }

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"NTHANHHAI 👻" <nthanhhai2909@gmail.com>', // sender address
            to: req.body.email, // list of receivers
            subject: 'Account Verification Token', // Subject line
            html: '<b>Reset password</b>'
                + ' <br/>'
                + '<span>Please clicking the link</span>'
                + '<br/>'
                + '<span>http://localhost:3001/forgot/username='+ req.body.email +  '</span>'
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                res.json({status: 'false', message:error});      
                return;
            }
            res.json({status: 'true'});
        });

    });
}

exports.resetpassword = (req, res) => {
    if(typeof req.body.email ===  'undefined'
        || typeof req.body.newPassword ===  'undefined'){
        res.json({status: 'false'})
    }

    User.findOne({'email': req.body.email}, 'email', (err, user) => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
        if(user === null){
            res.json({status: 'false'});
            return;
        }

        let hashedPassword = passwordHash.generate(req.body.newPassword);
        user.password = hashedPassword;
        
        user.save((err, data) => {
            if(err){
                res.json({status: 'false', message: err});
                return;
            }
            res.json({status: 'true'});
        })
    })

}

exports.getAddress =  (req, res) => {
    if(typeof req.params.email ===  'undefined'){
        res.json({status: 'false', message:'INPUT INVALID'})
        return;
    }

    User.findOne({'email': req.params.email}, {}, (err, user) => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
        if(user === null){
            res.json({status: 'false', message:'USER NOT FOUND'});
            return;
        }

        kPA.findOne({'_userID': user._id}, {}, (err, kpa) => {
            if(err){
                res.json({status: 'false', message: err});
                return;
            }
            if(kpa === null){
                res.json({status: 'false', message:'KPA INVALID'});
                return;
            }
            let data = Object.assign({}, {
                address: kpa.address,
            })

            res.json({status: 'true', data: data}); 
        });
    })
    
}

exports.getTransactionUnconfirmOfUser = (id) => {
    return new Promise(resolve => {
        transactionUnconfirm.findById(id, (err, docs) => {
            if(err){
                resolve(false);
                return;
            }
            resolve(docs);
        })
    })
}

exports.getBalance = async (req, res) => {
    if(typeof req.params.email ===  'undefined'){
        res.json({status: 'false', message:'INPUT INVALID'})
        return;
    }
    let userResult = await this.findUserByEmail(req.params.email);
    if(userResult === false){
        res.json({status: 'false', message:'USER NOT EXIST'})
        return;
    }
    res.json({status: 'true', 
        data: {
            actualBalance: userResult.actualBalance,
            availableBalance: userResult.availableBalance
        }
    })
}

exports.findKPAByUserID = (userID) => {
    return new Promise(resolve => {
        kPA.findOne({_userID: userID}, {}, (err, resultKPA) => {
            if(err){
                resolve( false);
            }
            if(resultKPA === null){
                resolve( false);
            }
            resolve(resultKPA);
        })
    })
}

exports.findUserByEmail = (email) => {
    return new Promise(resolve => {
        User.findOne({'email': email}, {}, (err, resultUser) => {
            if(err){
                resolve (false);
            }
            if(resultUser === null){
                resolve (false);
            }

            resolve(resultUser);
        })
    })
}

exports.sentKcoin = async (req, res) => {
    if(typeof req.body.addressTo === 'undefined'
        || typeof req.body.amount === 'undefined'
        || typeof req.body.email === 'undefined'){
            res.json({status: 'false'});
        }
        

        let depositInformations = [];
        depositInformations.push({
            address: req.body.addressTo,
            deposits: req.body.amount,
        })

        let userResult = await this.findUserByEmail(req.body.email);
        userResult.save(err => console.log(err));
        if(userResult === false){
            res.json({status: 'false'})
            return;
        }
        
        userResult.save(err => console.log(err));
        let token = randomstring.generate()
        
        let newTransactionUncofirm = new transactionUnconfirm({
            token: token,
            isVerified: false,
            depositInformations: depositInformations,
            _userID: userResult._id,
            status: 1
        });

        newTransactionUncofirm.save(async err => {
            if(err){
                res.json({status: 'false'})
            }
            let hanger = await transactionController.gethHangersByIDUser(userResult._id);
            console.log('hager', hanger);
            userResult.availableBalance = (Number(userResult.actualBalance) 
            - Number(hanger)).toString();
            userResult.save(err => {})
            // setup email data with unicode symbols
            let mailOptions = {
                from: '"NTHANHHAI 👻" <nthanhhai2909@gmail.com>', // sender address
                to: req.body.email, // list of receivers
                subject: 'Sent KCoin Verification ', // Subject line
                html: '<b>verify transaction</b>'
                    + ' <br/>'
                    + '<span>Please verify sent KCOIN by clicking the link</span>'
                    + '<br/>'
                    + '<span>http://localhost:3001/user/confirmtransaction/' + token +  '</span>'
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    res.json({status: 'false', message:error});      
                    return;
                }
                res.json({status: 'true', message:''});
            });
            res.json({status: 'true'});
        })     
}

exports.cofirmSentKcoin = async (req, res) => {
    if(typeof req.params.token === 'undefined'){
        res.json({status: 'false'});
    }
    let mtransacionUnconfirm = await this.findTransactionUnconfirm(req.params.token);
    if(mtransacionUnconfirm === false){
        res.json({status: 'false'});
        return;
    }

    mtransacionUnconfirm.status = 2;
    mtransacionUnconfirm.isVerified = true;
    let resUser = await this.findUserByID(mtransacionUnconfirm._userID);
    mtransacionUnconfirm.save(err => {
        if(err){
            res.json({status: 'false', message: err});
            return;
        }
        console.log('dmm', resUser.email)
        res.json({status: 'true', email: resUser.email});

    })

}

exports.findUserByID = (id) => {
    return new Promise(resolve => {
        User.findById(id, (err, res) => {
            if(err){
                resolve(false);
            }
            if(res === null){
                resolve(false)
                 return;
            }
            resolve(res)
            
        })
    })
}

exports.findTransactionUnconfirm = (mtoken) => {
    return new Promise(resolve => {
        transactionUnconfirm.findOne({token: mtoken}, {}, (err, result) => {
            if(err){
                resolve(false);
                return;
            }
            if(result === null){
                resolve(false);
                return;
            }
            if(result.isVerified === true){
                resolve(false);
                return;
            }
            resolve(result);
        })
    })
}
