'use strict'
var mongoose = require('mongoose');
var User = require('../models/UserModel');
var tokenVerified = require('../models/TokenVerifiedModel');
var kPA = require('../models/KeyPairAddressModel');
var randomstring = require("randomstring");
var passwordHash = require('password-hash');
const nodemailer = require('nodemailer');
var axios = require('axios')
var transporter = nodemailer.createTransport('smtps://nthanhhai2909%40gmail.com:missing456@smtp.gmail.com');
var transactionUtils = require('../utils/transaction');
var contract = mongoose.model('contract');
var transactionUnconfirm = require('../models/TransactionUnconfirmModel');
var userController = require('./UserController');

exports.getTotalTransactionUnconfirmByUserID = (id) => {
    return new Promise(resolve => {
        transactionUnconfirm
        .find({_userID: id}, {}, (err, docs) => {
            if(err){
                resolve(false);
                return;
            }
            resolve(docs.length);
        })
    })
}
exports.getTransactionUnconfirm = async (req, res) => {
    if(typeof req.body.email === 'undefined'
        || typeof req.body.amount === 'undefined'
        || typeof req.body.page === 'undefined'){
        res.json({status: 'false', message: 'inputs invalid'});
        return;
    }

    let mypage = parseInt(req.body.page);
    if(mypage < 1){
        res.json({status: 'false', mess: 'page < 0'});
        return;
    }

    let userRes = await userController.findUserByEmail(req.body.email);
    if(userRes === false){
        res.json({status: 'false'});
        return;
    } 

    let myamount = parseInt(req.body.amount);
    let skip = (mypage-1) * myamount; 
    let allRecord = await this.getTotalTransactionUnconfirmByUserID(userRes._id); 
    let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
    if(mypage > mtotalPage){
        res.json({status: 'false', message: 'page > toal'});
        return;
    }

    let myData = [];
    transactionUnconfirm
    .find({_userID: userRes._id})
        .limit(myamount)
        .skip(skip)
        .sort({date: 'descending'})
        .exec(async function(err, docs){
            if(err){
                res.json({status:'false'});
            }
            for(let i = 0; i < docs.length; i++){
                let obj = {
                    id: docs[i].token,
                    addressTo: docs[i].depositInformations[0].address,
                    amount: docs[i].depositInformations[0].deposits,
                    status: docs[i].status
                }
                myData.push(obj);
            }

            res.json({status: 'true', data: myData, totalPage: mtotalPage});
        })

}

exports.deleteTransactionUnconfirm = (req, res) => {
    if(typeof req.params.token === 'undefined'){
        res.json({status: 'false'});
        return;
    }
    transactionUnconfirm.findOne({token: req.params.token}, {}, async (err, docs) => {
        if(err){
            res.json({status: 'false'});
            return;
        }
        if(docs === null){
            res.json({status: 'false'});
            return;
        }
        if(docs.status !== 1){
            res.json({status: 'false'});
            return;
        }
        docs.remove(err => {
            if(err){
                res.json({status: 'false'});
            return;
            }
            res.json({status: 'true'});
        })

        let userID = docs._userID;

        
        let userResult = await userController.findUserByID(userID);
        userResult.availableBalance = (parseFloat(userResult.availableBalance) 
            + parseFloat(docs.depositInformations[0].deposits)).toString();
            userResult.save(err => {}); 
        
    })
}

exports.findTransactionUnConfirmByIDUser = (userID) => {
    return new Promise (resolve => {
        transactionUnconfirm.find({_userID: userID}, (err, resultTU) => {
            if(err){
                resolve (false);
            }
            resolve(resultTU);
        })
    })
}

exports.findKPAByAddress = (maddress) => {
    return new Promise(resolve => {
        kPA.findOne({address: maddress}, {}, (err, result) => {
            if(err){
                resolve(false);
                return;
            }
            if(result === null){
                resolve(false);
                return;
            }
            resolve(result);
        })
    })
}

exports.findUserByID = (id) => {
    return new Promise(resolve => {
        User.findById(id, (err, res) => {
            if(err){
                resolve(false);
            }
            if(res === null){
                resolve(false)
                 return;
            }
            resolve(res)
            
        })
    })
}


// 1 - noi bo ; 2 - ngoai ; -1 -  loi 
exports.checkAddress = (maddress) => {
    return new Promise(resolve => {
        kPA.find({address:maddress}, {}, (err, results) => {
            if(err){
                resolve(-1);
                return;
            }
            if(results.length === 0){
                resolve(2);
                return;
            }
            resolve(1);
            
        })
    })
    
}
exports.getALlTransactionAwaitHandle = () => {
    return new Promise(resolve => {
        transactionUnconfirm.find({status: Number(2)}, {} , (err, results) => {
            if(err){
                resolve(false);
                return;
            }
            if(results === null){
                resolve(false);
                return;
            }   
            resolve(results);
            
        })
    })
}

exports.handleListTransactionInternal = (listTransaction) => {
    return new Promise(async resolve => {
        listTransaction.forEach(async mtransaction => {
            let addressTo = mtransaction.depositInformations[0].address;
            let amount = mtransaction.depositInformations[0].deposits;
            // // tru tien
            let userRes = await userController.findUserByID(mtransaction._userID);
            if(userRes === false){
                resolve(false);
                return;
            }
            userRes.actualBalance = (parseFloat(userRes.actualBalance) - parseFloat(amount)).toString();
            // userRes.availableBalance = (parseFloat(userRes.availableBalance) - parseFloat(amount)).toString();
            userRes.save(err => console.log(err));
            let kPARes = await this.findKPAByAddress(addressTo);
            if(kPARes === false){
                return;
            }
            

            let userReceive = await userController.findUserByID(kPARes._userID);
            if(userReceive === false){return}
            userReceive.availableBalance = (parseFloat(userReceive.actualBalance) + parseFloat(amount)).toString();
            userReceive.actualBalance = (parseFloat(userReceive.actualBalance) + parseFloat(amount)).toString();
            userReceive.save(err => console.log(err));
            mtransaction.status = 3;
            mtransaction.save(err => console.log(err));

            // tao giao dich tien noi bo
            var newContract = new contract({
                address: addressTo,
                hashTransactionID: 'undefined',
                outputIndex: -1,
                amount:amount,
                isUse: true,
            });
            console.log('----------------------', newContract);
            newContract.save(err => {
                if(err){
                    console.log('alooooooooooooooooo', err)
                }
            });
        }) 
        resolve(true);
    })
}
exports.handleListTransactionOutside = (listTransaction) => {
    return new Promise(async resolve => {
        let depositInformations = [];
        listTransaction.forEach((element, index) => {
            depositInformations.push(element.depositInformations);
        })
        let res = await transactionUtils.tranfer(depositInformations);
        resolve(true);
        
    })
}

exports.handleTransactionAwait = () => {
    return new Promise(async resolve => {

        let listTransactionAwait = await this.getALlTransactionAwaitHandle();
        if(listTransactionAwait === false){
            return;
        }
        let listTransactionOutside = [];
        let listTransactionInteral = [];
        for(let i = 0; i < listTransactionAwait.length; i++){
            if((await this.checkAddress(listTransactionAwait[i].depositInformations[0].address)) === 1){              
                 listTransactionInteral.push(listTransactionAwait[i]);
                
            }
            if((await this.checkAddress(listTransactionAwait[i].depositInformations[0].address)) === 2){
                 listTransactionOutside.push(listTransactionAwait[i]);
            }
        }
        
        await this.handleListTransactionInternal(listTransactionInteral);
        await this.handleListTransactionOutside(listTransactionOutside);

        resolve(true);

    })
}


exports.gethHangersByIDUser = (id) => {
    return new Promise(resolve => {
        transactionUnconfirm.find({_userID: id, 
            $or:[ {'status':1}, {'status': 2 }] }, (err, docs) => {
            if(err){
                resolve(false);
                return;
            }
      
            let total = 0;
            for(let i = 0; i < docs.length; i++){
                total += parseFloat(docs[i].depositInformations[0].deposits);
            }
        
            resolve(total)
        })
    })
}

exports.getTransactionReivece = async (req, res) => {
    if(typeof req.body.email === 'undefined'
        || typeof req.body.amount === 'undefined'
        || typeof req.body.page === 'undefined'){
            res.json({status: 'false'})
            return;
    }
    let userRes = await userController.findUserByEmail(req.body.email);
    if(userRes === false){
        res.json({status: 'false'});
        return;
    }
    let kPARes = await this.findKPAbyUserID(userRes._id);
    if(kPARes === false){
        res.json({status: 'false'});
        return;
    }
    let mypage = parseInt(req.body.page);
    if(mypage < 1){
        res.json({status: 'false'});
        return;
    }
    let myamount = parseInt(req.body.amount);
    let skip = (mypage-1) * myamount;  
    let allRecord = await this.getTotalPageTransactionReiceve(kPARes.address);
    let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
    if(mypage > mtotalPage){
        res.json({status: 'false'});
        return;
    }
    contract.
        find({address: kPARes.address})
        .limit(myamount)
        .skip(skip)
        .sort({date: 'descending'})
        .exec(function(err, docs){
            if(err){
                res.json({status:'false'});
            }
            res.json({status: 'true', data: docs, totalPage: mtotalPage});
        })

}

exports.getTotalPageTransactionReiceve = (maddress) => {
    return new Promise(resolve => {
        resolve(contract.count({address:maddress}));
    })

}
exports.getTotalPageTransactionTranfer = (idUser) => {
    return new Promise(resolve => {
        resolve (transactionUnconfirm.count({_userID: idUser}))
    })
}

exports.findContractsByAddress = (maddress) => {
    return new Promise(resolve => {
        contract.find({address: maddress}, {}, (err, docs) => {
            if(err){
                resolve(false);
                return;
            }
            resolve(docs);
        })
    })
}

exports.getTransactionTranfer = async (req, res) => {
    if(typeof req.body.email === 'undefined'
        || typeof req.body.amount === 'undefined'
        || typeof req.body.page === 'undefined'){
            
            res.json({status: 'false',mess: 'input false'})
            return;
    }

    let userRes = await userController.findUserByEmail(req.body.email);
    if(userRes === false){
        res.json({status: 'false',mess: 'user not found'});
        return;
    }
    
    let mypage = parseInt(req.body.page);
    if(mypage < 1){
        res.json({status: 'false', mess: 'page < 0'});
        return;
    }

    let myamount = parseInt(req.body.amount);
    let skip = (mypage-1) * myamount; 
    let allRecord = await this.getTotalPageTransactionTranfer(userRes._id); 
    let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
    if(mypage > mtotalPage){
        res.json({status: 'false', message: 'page > toal'});
        return;
    }
    
    transactionUnconfirm
        .find({_userID: userRes._id, status: 3})
        .limit(myamount)
        .skip(skip)
        .sort({date: 'descending'})
        .exec(function(err, docs){
            if(err){
                res.json({status:'false'});
            }
            res.json({status: 'true', data: docs, totalPage: mtotalPage});
        })
}

exports.findKPAbyUserID = (id) => {
    return new Promise(resolve => {
        kPA.findOne({_userID: id}, {}, (err, doc) => {
            if(err){
                resolve(false);
                return;
            }
            if(doc === null){
                resolve(false);
                return;
            }
            resolve(doc)
        })
    })
}