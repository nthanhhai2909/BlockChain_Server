'use strict'
var mongoose = require('mongoose');
var kPA = require('../models/KeyPairAddressModel');

exports.addKeyPairforUser = (req, res) => {


    if((typeof req.body._userID == 'undefined')
         || (typeof req.body.publicKey == 'undefined')
            || typeof req.body.privateKey == 'undefined'
            || typeof req.body.address == 'undefined'){
        res.json({status: 'false', message: 'INPUT INVALID'});
        return;
    }

    var newKPA = new kPA({
        _userID: req.body._userID,  
        publicKey: req.body.publicKey,   
        privateKey: req.body.privateKey,
        address: req.body.address,
    });
    newKPA.save((err, newkPA) => {
        if(err){
            res.json({status: 'false', message: err});
        }
        res.json({status: 'true', message: err});

    })
}