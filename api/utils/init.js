
const axios = require('axios');
var mongoose = require('mongoose');
var block = require('../models/BlockModel');
var transaction = require('../models/TransactionModel');
var contract = require('../models/ContractModel');
var kPA = require('../models/KeyPairAddressModel');
var transactionUnconfirm = require('./transaction')
var transactionUtils = require('../utils/transaction');


exports.countBlockInDataBase = () => {
    return new Promise (resolve => {
        resolve(block.count({}));
    })
}


exports.dataSynchronization = () => {
    return new Promise(async (resolve) => {
        let countBlock = await this.countBlockInDataBase();
        console.log('so block co trong database : ', countBlock)
        var blockInfor = await axios.get('https://api.kcoin.club/blocks/');
        let temp = Number(blockInfor.headers[Object.keys(blockInfor.headers)[6]]);
        let numberBlock = Number(blockInfor.headers[Object.keys(blockInfor.headers)[6]]);
        numberBlock -= countBlock;
        let index = Math.ceil(numberBlock / 100);
        for (let i = 0; i < index; i++) {
            
            console.log('index', i)
            let blockIndex = await axios.get('https://api.kcoin.club/blocks/?' +
                'limit=100&' + 'offset=' + parseInt(countBlock+ i * 100) + '&order=0');
            for (let j = 0; j < blockIndex.data.length; j++) {
                if(countBlock > 0){
                    await this.processBlock(blockIndex.data[j], 2);
                }
                else{
                    await this.processBlock(blockIndex.data[j], 1);
                }
            }
        }
        // let blockIndex = await axios.get('https://api.kcoin.club/blocks/?' +
        //     'limit=100&' + 'offset=' + index * 100 + '&order=0');

        // for (let j = 0; j < blockIndex.data.length; j++) {
        //     if(countBlock > 0){
        //         await this.processBlock(blockIndex.data[j], 2);
        //     }
        //     else{
        //         await this.processBlock(blockIndex.data[j], 1);
        //     }
        // }
        if(countBlock === 0){
            transactionUtils.caculatorActualAccount();
        }
        resolve(true);
    })
}


exports.saveBLock = (hash, nonce, version, timestamp, difficulty) => {
    console.log('1', hash);
    console.log('2',nonce)
    console.log('2',version)
    console.log('2',timestamp)
    console.log('2', difficulty)
    if (typeof hash === 'undefined'
        || typeof nonce === 'undefined'
        || typeof version === 'undefined'
        || typeof timestamp === 'undefined'
        || typeof difficulty === 'undefined') {
        console.log('block false')
    }
    return new Promise(resolve => {
        var newBlock = new block({
            hash: hash,
            nonce: nonce,
            version: version,
            timestamp: timestamp,
            difficulty: difficulty,
        });

        newBlock.save((err, resultBlock) => {
            if (err) {
                console.log('err', err);
                resolve(false);
                return;
            }
            resolve(true);
        })
    })
}

exports.saveTransaction = (blockHash, mtransaction) => {
    console 
    if (typeof blockHash === 'undefined'
        || typeof mtransaction === 'undefined') {
        console.log('block false')
    }
    return new Promise(resolve => {
        var newTransaction = new transaction({
            blockHash: blockHash,
            hash: mtransaction.hash,
            inputs: mtransaction.inputs,
            outputs: mtransaction.outputs,
        });
        newTransaction.save((err, resultTransaction) => {
            if (err) {
                console.log('err1', err);
                resolve(false);
            }
            resolve(true);
        });
    })


}

exports.findKPAByAdress = (maddress) => {
    return new Promise(resolve => {
        kPA.findOne({address: maddress}, {}, (err, result) => {
            if(err){
                resolve(false);
                return;
            }
            if(result === null){
                resolve(false);
                return;
            }
            resolve(true);
        })
    })
}

exports.saveContract = (newContract) => {
    return new Promise( resolve => {
        newContract.save((err, data) => {
            if(err){
                resolve(false);
                return;
            }
            resolve(true)
        })
    })
}
exports.updateUseContract = (mcontract) => {
    console.log('update 1', mcontract)
    return new Promise(resolve => {
        mcontract.save(err => {
            if(err){
                resolve(false);
            }
            resolve(true);
        })
    })
}
exports.updateContracts = (mtransaction) => {
    return new Promise(async resolve => {
        for(let i = 0; i <  mtransaction.outputs.length; i++){
            let addressReceive = mtransaction.outputs[i].lockScript.slice(4,
                mtransaction.outputs[i].lockScript.length);
            let amount = mtransaction.outputs[i].value;
            var newContract = new contract({
                address: addressReceive,
                hashTransactionID: mtransaction.hash,
                outputIndex: i,
                amount: amount
            })
            let res = await this.saveContract(newContract);
            resolve(true)

        }

        mtransaction.inputs.forEach((element, index) => {
            contract.findOne({
                hashTransactionID: element.referencedOutputHash,
                outputIndex: element.referencedOutputIndex
            }, {}, async(err, resultContract) => {
                if (err) {
                    console.log('err3', err);
                    resolve(false);
                }
                if (resultContract !== null) {
                    resultContract.isUse = true;
                    await this.updateUseContract(resultContract);
                }
            })
        })
        resolve(true);
    })
}


//type = 1 dong bo; type = 2 socket
exports.processBlock = (block, type) => {
    return new Promise(async (resolve) => {
        let resultSaveBLock = await this.saveBLock(block.hash, block.nonce,
            block.version, block.timestamp, block.difficulty);

        if (resultSaveBLock === false) {
            console.log('save block false')
            resolve(false);
        }
        for (let j = 0; j < block.transactions.length; j++) {
            let resultSaveTransaction = await
                this.saveTransaction(block.hash, block.transactions[j]);

            if (resultSaveTransaction === false) {
                console.log('save transaction false')
                resolve(false);
            }
            
            if(type === 2){
                let resUpdateTransactionUncofirmRes = await transactionUtils
                    .updateContractUnconfirm(block.transactions[j]);
                console.log('update contract', resUpdateTransactionUncofirmRes);
            }
            
            let resultUpdateContractForUser = await this.updateContracts(block.transactions[j]);
            if (resultUpdateContractForUser === false) {
                console.log('update contract false')
                resolve(false);
            }
        }

        resolve(true);

    })
}



