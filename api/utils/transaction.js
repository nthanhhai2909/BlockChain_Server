
var mongoose = require('mongoose');
var User = require('../models/UserModel');
var tokenVerified = require('../models/TokenVerifiedModel');
var kPA = require('../models/KeyPairAddressModel');
var block = mongoose.model('block');
var transaction = require('../models/TransactionModel');
var contract = require('../models/ContractModel');
var transactionUncofirm = require('../models/TransactionUnconfirmModel')
var axios = require('axios');
const ursa = require('ursa');
const HASH_ALGORITHM = 'sha256';
var userController = require('../controllers/UserController')
var ADDRESS_ADMIN = 'a6e4ee0581a2a94d0f1c65156d62d6956185e7da596435a0810467c3b4c9818d';
var transactionController = require('../controllers/TransactionController')

exports.tranfer =  (depositInformations) => {
    return new Promise(async resolve => {
        if (typeof depositInformations == 'undefined' ){
            resolve(false);
        }
        if(depositInformations.length === 0){
            return;
        }
        let contracts = await this.getALlContractUse();
        let totalAmount = 0;

        depositInformations.forEach(element => {
            totalAmount += parseFloat(element[0].deposits);
        })

        if (contracts === false) {
            resolve (false);
        }

        let mcontractFormat = [];
        contracts.forEach(element => {
            element.forEach(e => {
                mcontractFormat.push(e);
            })
        })

        let arrIndex = this.selectContractsconditional(mcontractFormat, parseFloat(totalAmount));
        if (arrIndex === false) {
            resolve (false);
        }

        let referenceOutputsHashes = [
        ];

        // Generate transacitons
        let bountyTransaction = {
            version: 1,
            inputs: [],
            outputs: []
        };

        
        let totalContractSelect = 0;

        
        arrIndex.forEach(element => {
            referenceOutputsHashes.push({
                referencedOutputHash: mcontractFormat[element].hashTransactionID,
                referencedOutputIndex: mcontractFormat[element].outputIndex,
                address: mcontractFormat[element].address   
            });
            //  update contract 
            mcontractFormat[element].awaitConfirm = true;
            mcontractFormat[element].save(err => {});
            totalContractSelect += parseFloat(mcontractFormat[element].amount);
        })
        let keys = [];

        for(let i = 0; i < referenceOutputsHashes.length; i ++){
            bountyTransaction.inputs.push({
                referencedOutputHash: referenceOutputsHashes[i].referencedOutputHash,
                referencedOutputIndex: referenceOutputsHashes[i].referencedOutputIndex,
                unlockScript: ''
            });
            let key =  await this.findKPAByAddress(referenceOutputsHashes[i].address);
            keys.push(key);
        }

       

        if ((totalContractSelect - totalAmount) > 0) {
            bountyTransaction.outputs.push({
                value: totalContractSelect - parseFloat(totalAmount),
                lockScript: 'ADD ' + ADDRESS_ADMIN
            });
        }

        depositInformations.forEach(element => {
            bountyTransaction.outputs.push({
                value: parseFloat(element[0].deposits),
                lockScript: 'ADD ' + element[0].address
            });
        });
        console.log('truoc', bountyTransaction);
        this.sign(bountyTransaction, keys);
        console.log('sau', bountyTransaction);

        axios.post('https://api.kcoin.club/transactions', {
            inputs: bountyTransaction.inputs,
            outputs: bountyTransaction.outputs,
            version: bountyTransaction.version
        })
        .then(res => {
            console.log('res', res)
            if(res.status === 200){
                resolve(true)
            }
            else{
                resolve(false);
            }
        })
        .catch(err => {console.log('loi', err); resolve(false)})   
    })
    resolve(true);

}
exports.findContractByIDAndIndexOutput = (mHashTransactionID, mOutputIndex) => {
    console.log('mHashTransactionID', mHashTransactionID);
    console.log('mOutputIndex', mOutputIndex);

    return new Promise(resolve => {
        contract.findOne({
            hashTransactionID: mHashTransactionID,
            outputIndex: mOutputIndex,
        }, {}, (err, result) => {
            if(err){
                resolve(false);
                return;
            }
            if(result === null){
                resolve(false);
                return;
            }
            resolve(result);
        })
    })
}
exports.checkTransactionOfSentSystem = (inputs) => {
    return new Promise(async resolve => {
        for(let i = 0; i < inputs.length; i++){
            let resContract = await this.
                findContractByIDAndIndexOutput(inputs[i].referencedOutputHash, 
                inputs[i].referencedOutputIndex);
                console.log('----------' + resContract)
            if(resContract === false){
                resolve(false);
                return;
            }
            let kPARes = await this.findKPAByAddress(resContract.address);
            if(kPARes !== false){
                resolve(true);
                return;
            }
            else{
                resolve(false)
            }
        }
        resolve(false)
    })
}

exports.findTransactionUnconfirmByAddressTo = (addressTo) => {
    return new Promise(resolve => {
        transactionUncofirm.findOne({'depositInformations.address': addressTo, status: 2}, {}, (err, result) => {
            if(err){
                resolve(false);
                return;
            }
            if(result === null){
                resolve(false);
                return;
            }
            
            resolve(result);
        })
    })
}

// continue........................................................................
exports.updateconfirm = (newConfirm) => {
    return new Promise(resolve => {
        newConfirm.save(err => {
            if(err){
                resolve(false);
                return;
            }
            resolve(true);
        })
    })
}
exports.saveBalance = (user) => {
    return new Promise(async resolve => {
        await user.save(err => {
            if(err){
                console.log(err)
                resolve(false);
            }
        });
        resolve(true);
    })
}

exports.caculatorActualAccount =  () => {
    return new Promise(async resolve => {
        let contracts = await this.getALlContractUse();
        for(let j = 0; j < contracts.length; j++){
            for(let i = 0; i < contracts[j].length; i++){
                let addressReceive =  contracts[j][i].address;
                let amount = contracts[j][i].amount;
                let resKPA = await transactionController.findKPAByAddress(addressReceive);
                if(resKPA !== false){
                    let resUser = await transactionController.findUserByID(resKPA._userID);
                    if(resUser !== false){
                        let myBlance = parseFloat(resUser.actualBalance);
                        let newBalance = myBlance + parseFloat(amount);
                        resUser.actualBalance = newBalance.toString();
                        resUser.availableBalance = newBalance.toString();
                        await this.saveBalance(resUser);
                    }
                }
            }
        }
        
    })
}

exports.updateContractUnconfirm = (mtransaction) => {
    return new Promise( async resolve => {
        let check = await this.checkTransactionOfSentSystem(mtransaction.inputs);
        console.log('check', check)
        //kiem tra cap nhap giao dich cho user
        if(check === true){
            for(let  i = 0; i < mtransaction.outputs.length; i++){
                let addressReceive =
                     mtransaction.outputs[i].lockScript.slice(4,
                         mtransaction.outputs[i].lockScript.length);
                    if(addressReceive === ADDRESS_ADMIN){
                        continue;
                    }

                    let transactionUnconfirmRes = await 
                        this.findTransactionUnconfirmByAddressTo(addressReceive);
                    if(transactionUnconfirmRes !== false){
                        transactionUnconfirmRes.status = 3;
                        transactionUnconfirmRes.outputIndex = i;
                        transactionUnconfirmRes.hashTransactionID = mtransaction.hash;
                        //---------------------------------------
                        let userSentUpdate = await userController.
                            findUserByID(transactionUnconfirmRes._userID);
                        console.log('user sent', userSentUpdate)
                        userSentUpdate.actualBalance = (parseFloat(userSentUpdate.actualBalance) 
                            - transactionUnconfirmRes.depositInformations[0].deposits).toString();
                        userSentUpdate.save(err => {});
                        console.log('confirm', transactionUnconfirmRes)
                        let res = await this.updateconfirm(transactionUnconfirmRes);
                        if(res === false){console.log('update transaction unconfirm false')}
                    }     
            }
        }
        //cong tien cho user khi duoc chuyen tien tu mot transaction bat ky
        for(let i = 0; i <  mtransaction.outputs.length; i++){
            let addressReceive = mtransaction.outputs[i].lockScript.slice(4,
                 mtransaction.outputs[i].lockScript.length);
            let amount = mtransaction.outputs[i].value;
            let kPARes = await this.findKPAByAddress(addressReceive);
            if(kPARes === false){
                resolve(false); 
                return;
            }

            let userRes = await userController.findUserByID(kPARes._userID);
            if(userRes === false){
                resolve(false); 
                return;
            }
           
            let newActualBalance = parseFloat(userRes.actualBalance) + parseFloat(amount);
            userRes.actualBalance = newActualBalance.toString();
            let newAvailableBalance = parseFloat(userRes.availableBalance) + parseFloat(amount);
            userRes.availableBalance = newAvailableBalance.toString();
            resSaveUser = await this.updateUser(userRes);
            if(resSaveUser === false){
                resolve(false); 
                return;
                console.log('save user false');
            }
        }
        resolve(true)
    })
}

exports.updateUser = (newUser) => {
    return new Promise( resolve => {
        newUser.save(err => {
            if(err){
                resolve(false);
                return;
            }
            resolve(true);
        })
    })
    
}


exports.getALlContractUse = () => {
    return new Promise(async resolve => {
        let kPAs = await this.getALlKPA();
        if(kPAs === false){
            resolve(false);
            return;
        }
        let contracts = [];
        for(let i = 0; i < kPAs.length; i++){
            let temp = await this.getContract(kPAs[i].address);
            if(temp !== false){
                contracts.push(await this.getContract(kPAs[i].address));
            }
        }
        resolve(contracts);
    })
}

exports.getContract = (address) => {
    return new Promise((resolve) => {
        if (typeof address === 'undefined') {
            resolve(false);
            return;
        }
        contract.find({ address: address, isUse: false}, (err, contracts) => {
            if (err) {
                console.log(err);
                resolve(false);
                return;
            }

            if (contracts.length === 0) {
                resolve(false);
                return;
            }
            resolve(contracts);
        })
    });
}

exports.getALlKPA = () => {
    return new Promise(async resolve => {
        kPA.find({}, (err, results) => {
            if(err){ resolve(false); return}
            resolve(results)
        })
    })
}

exports.selectContractsconditional = (contracts, amount) => {
    let arr = [];
    for (let i = 0; i < contracts.length; i++) {
        let myAmount = 0;
        myAmount += parseFloat(contracts[i].amount);

        arr.push(i);
        if (myAmount >= amount) {
            break;
        }
    }
    return arr;

}

exports.findKPAByAddress = (maddress) => {
    return new Promise(resolve =>{
        console.log('maddress', maddress)
        kPA.findOne({address: maddress}, {}, (err, results) => {
            if(err){
                resolve(false);
                return;
            }
            if(results === null){
                resolve(false);
                return;
            }
            resolve(results);
        })
    })
}

exports.readKey = (_userID) => {
    return new Promise(resolve => {
        if (typeof _userID == 'undefined') {
            resolve(false);

        }
        kPA.findOne({ _userID: _userID }, {}, (err, resultKPA) => {
            if (err) {
                console.log('err', err);
                resolve(false);


            }
            if (resultKPA === null) {
                resolve(false);
            }

            resolve(Object.assign({}, {
                publicKey: resultKPA.publicKey,
                privateKey: resultKPA.privateKey,
                address: resultKPA.address,
            }));
        })

    })

}



// Convert a transaction to binary format for hashing or checking the size
exports.toBinary = function (transaction, withoutUnlockScript) {
    let version = Buffer.alloc(4);
    version.writeUInt32BE(transaction.version);
    let inputCount = Buffer.alloc(4);
    inputCount.writeUInt32BE(transaction.inputs.length);
    let inputs = Buffer.concat(transaction.inputs.map(input => {
        // Output transaction hash
        let outputHash = Buffer.from(input.referencedOutputHash, 'hex');
        // Output transaction index
        let outputIndex = Buffer.alloc(4);
        // Signed may be -1
        outputIndex.writeInt32BE(input.referencedOutputIndex);
        let unlockScriptLength = Buffer.alloc(4);
        // For signing
        if (!withoutUnlockScript) {
            // Script length
            unlockScriptLength.writeUInt32BE(input.unlockScript.length);
            // Script
            let unlockScript = Buffer.from(input.unlockScript, 'binary');
            return Buffer.concat([outputHash, outputIndex, unlockScriptLength, unlockScript]);
        }
        // 0 input
        unlockScriptLength.writeUInt32BE(0);
        return Buffer.concat([outputHash, outputIndex, unlockScriptLength]);
    }));
    let outputCount = Buffer.alloc(4);
    outputCount.writeUInt32BE(transaction.outputs.length);
    let outputs = Buffer.concat(transaction.outputs.map(output => {
        // Output value
        let value = Buffer.alloc(4);
        value.writeUInt32BE(output.value);
        // Script length
        let lockScriptLength = Buffer.alloc(4);
        lockScriptLength.writeUInt32BE(output.lockScript.length);
        // Script
        let lockScript = Buffer.from(output.lockScript);
        return Buffer.concat([value, lockScriptLength, lockScript]);
    }));
    return Buffer.concat([version, inputCount, inputs, outputCount, outputs]);
};

// Sign transaction
exports.sign = function (transaction, keys) {

    let message = this.toBinary(transaction, true);
    transaction.inputs.forEach((input, index) => {
        let key = keys[index];
        let signature = this.utilsSign(message, key.privateKey);
        // Genereate unlock script
        input.unlockScript = 'PUB ' + key.publicKey + ' SIG ' + signature;

    });


};

exports.utilsSign = function (message, privateKeyHex) {
    // Create private key form hex
    let privateKey = ursa.createPrivateKey(Buffer.from(privateKeyHex, 'hex'));
    // Create signer
    let signer = ursa.createSigner(HASH_ALGORITHM);
    // Push message to verifier
    signer.update(message);
    // Sign
    return signer.sign(privateKey, 'hex');
};

exports.getTransactionUncofirmIsVeri = () => {
    return new  Promise(async (resolve) => {
        transactionUncofirm.find({status: 2}, {}, (err, results) => {
            if(err){
                console.log(err);
                resolve(false);
            }
            resolve(results);
        })
    })
}

exports.caculatorAmount = (listTransactionUncofirm) => {
    listTransactionUncofirm.forEach(element => {
        let total = 0;
        total += parseFloat(element.depositInformations[0].deposits);
    })
    return total;
}




