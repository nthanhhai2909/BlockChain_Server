'use strict'
var mongoose = require('mongoose');
var User = require('../models/UserModel');

exports.getAmoutUser = () => {
    return User.count({});
}
exports.getAllUser = () => {
    return new Promise(resolve => {
        User.find({}, (err, docs) => {
            resolve(docs);
        })
    })
}

exports.getBalanceSystem = () => {
    return new Promise(async resolve => {
        let allUser = await this.getAllUser();
        let totalAvailableBalance = 0;
        let totalActualBalance = 0;
        for(let i = 0; i < allUser.length; i++){
            totalAvailableBalance += parseFloat(allUser[i].actualBalance);
            totalActualBalanc += parseFloat(allUser[i].availableBalance);
        }
        resolve (Object.assign({}, {actualBalance: totalActualBalance, availableBalance: totalAvailableBalance}));
    })
}

exports.getAllInforUser = (offset, limit) => {
    return new Promise (resolve => {
        User.paginate({}, {offset: offset, limit: limit}, (err, docs) => {
            resolve(docs)
        })
    })
}