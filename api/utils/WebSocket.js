
const WebSocket = require('ws');
const restify = require('restify');
var init = require('./init');
// HTTP API server
const corsM = require('restify-cors-middleware');
var EventEmitter = require('events').EventEmitter
const events = new EventEmitter();
const transactionController = require('../controllers/TransactionController')
exports.syncKcoinBlockChain = () => {
    
    const ws = new WebSocket('wss://api.kcoin.club/', {
        origin: 'https://api.kcoin.club'
    });

    ws.on('open', function open() {
        console.log('connected');
        setInterval(() => { ws.send(Date.now()); }, 30000);

    });

    ws.on('close', function close() {
        console.log('disconnected');
    });

    ws.on('message', async function incoming(data) {
    var obj = JSON.parse(data);
    console.log('data sent', obj);
    if(obj.type === 'transaction'){
        return;
    }
    console.log('process block, ', await init.processBlock(obj.data, 2))
    console.log('process transaction unconfirm', await transactionController.handleTransactionAwait());
    // if(!res){
    //     console.log('process block thanh cong')
    //     events.emit('block', );
    // }
    });
}

exports.serverListen = () => {
    const app = restify.createServer();
    const cors = corsM({
        origins: process.env.CORS_ALLOW_ORIGINS ? process.env.CORS_ALLOW_ORIGINS.split(',') : [],
        allowHeaders: ['Authorization', 'Content-Type'],
        exposeHeaders: ['Content-Length', 'X-Total-Count'],
        preflightMaxAge: 600
    });

    // CORS
    app.pre(cors.preflight)
    app.use(cors.actual);

    // WS server
    const wss = new WebSocket.Server({ server: app.server });

    // Setup body parser and query parser
    app.use(restify.plugins.queryParser({
        mapParams: true
    }));
    app.use(restify.plugins.bodyParser({
        mapParams: true
    }));


    // Broadcast events
    wss.broadcast = (data) => {
        console.log('hihi', data)
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send(JSON.stringify(data));
            }
        });
    };
    
    events.on('block', async () => {
        wss.broadcast({
            type: 'update_balance',
        });
    });

}

